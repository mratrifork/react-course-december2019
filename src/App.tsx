import React, { useState } from "react"
import "./App.css"
import { MovieList } from "./pages/movies/MovieList"
import datasource from "./data/data.json"
import { Movie } from "./data/models/Movie"
import { MovieDetail } from "./pages/movies/MovieDetails"
import { Switch, Route, Redirect } from "react-router-dom"

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route
            path="/movies"
            exact={true}
            render={routeProps => (
              <MovieList
                movies={datasource.movies as Movie[]}
                {...routeProps}
              />
            )}
          />
          <Route
            path="/movies/:id"
            render={routeProps => {
              const movie = datasource.movies.find(
                movie => movie.id == routeProps.match.params.id
              )

              if (movie) {
                return <MovieDetail movie={movie} {...routeProps} />
              } else {
                console.log("No movies matching id", routeProps.match.params.id)
                return <Redirect to="/movies" />
              }
            }}
          />
        </Switch>
      </header>
    </div>
  )
}

export default App
