import React from "react"
import { Movie } from "../../data/models/Movie"
import { RouteComponentProps } from "react-router"

export interface IMovieDetailsProps {
  movie: Movie
}

export const MovieDetail: React.FC<IMovieDetailsProps &
  RouteComponentProps> = props => {
  return (
    <div>
      <img src={props.movie.posterUrl} alt={props.movie.title} />
      <h4>{props.movie.title}</h4>
      <h4>{props.movie.actors}</h4>
      <h4>{props.movie.plot}</h4>
      <input
        type="button"
        value="Go back"
        onClick={() => props.history.goBack()}
      />
    </div>
  )
}
