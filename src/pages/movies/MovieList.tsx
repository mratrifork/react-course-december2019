import React from "react"
import "./movie.css"
import { Movie } from "../../data/models/Movie"
import { MovieListItem } from "./MovieListItem"
import { RouteComponentProps } from "react-router"

export interface IMovieListProps {
  movies: Movie[]
}

export const MovieList: React.FC<IMovieListProps &
  RouteComponentProps> = props => {
  return (
    <div>
      {props.movies.map(movie => (
        <MovieListItem
          key={movie.id}
          title={movie.title}
          posterUrl={movie.posterUrl}
          onClick={(): void => props.history.push(`/movies/${movie.id}`)}
        />
      ))}
    </div>
  )
}
