import React from "react"

export interface IMovieListItemProps {
  title: string
  posterUrl: string
  onClick: () => void
}

export const MovieListItem: React.FC<IMovieListItemProps> = props => {
  return (
    <div className="MovieListItem" onClick={() => props.onClick()}>
      <small>{props.title}</small>
      <img src={props.posterUrl} alt={props.title} />
    </div>
  )
}
