import React from "react"

export const Logo: React.FC<{ logoSrc: string }> = props => (
  <img src={props.logoSrc} className="App-logo" alt="logo" />
)
