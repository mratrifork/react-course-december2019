import React from "react"

export const Text: React.FC = props => <p>{props.children}</p>
