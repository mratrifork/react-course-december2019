export type Movie = {
  id: number
  title: string
  posterUrl: string
  plot: string
  actors: string
}
